from django.shortcuts import render, redirect, HttpResponse
from scipy.stats import norm
import math

def index(request):
  return render(request, 'index.html')

def calculate(request):
  if request.method == 'POST':
    try:
      #Conversion Rate of Control
      convRateControl = float(request.POST['convControl'])/float(request.POST['visitorControl'])
      #Conversion Rate of Variation
      convRateVariation = float(request.POST['convVariation'])/float(request.POST['visitorVariation'])
      #Standard Error of Control
      standardErrControl = math.sqrt((convRateControl * (1 - convRateControl)/float(request.POST['visitorControl'])))
      #Standard Error of Variation
      standardErrVariation = math.sqrt((convRateVariation * (1 - convRateVariation)/float(request.POST['visitorVariation'])))
      #Calulating z-Value
      zValue = (convRateControl - convRateVariation) / math.sqrt(math.pow(standardErrControl, 2) + math.pow(standardErrVariation, 2))
      #Calualting p-Value
      if float(request.POST['visitorControl']) > float(request.POST['visitorVariation']):
        pValue = norm.cdf(zValue)
      else:
        pValue = 1 - norm.cdf(zValue)
      pVal = round(pValue, 3)
    except:
      pVal = 'NaN'
    return HttpResponse(pVal)
  else:
    return redirect('/')
