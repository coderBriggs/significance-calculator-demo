$(document).ready(function() {
  $(document).on('submit', '#calculator', function(e) {
    e.preventDefault();
    if($('#visitorControl').val() == '' || $('#visitorVariation').val() == '' || $('#convControl').val() == '' || $('#convVariation').val() == ''){
      alert("Please enter numbers in all control and variation fields.")
    } else if ($('#visitorControl').val() < 15) {
      alert("There must be at least 15 control trials for this tool to produce any results.")
    } else if ($('#visitorVariation').val() < 15) {
      alert("There must be at least 15 variation trials for this tool to produce any results.")
    } else {
      $.ajax({
        type:'POST',
        url:'/calculate',
        async:true,
        data:{
          visitorControl:$('#visitorControl').val(),
          visitorVariation:$('#visitorVariation').val(),
          convControl:$('#convControl').val(),
          convVariation:$('#convVariation').val(),
          csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success:function(res) {
          $('#pValue').html(res);
          if(parseFloat(res) < 0.05) {
            $('#significance').html('Yes!');
          } else {
            $('#significance').html('No');
          }
          $('#results').slideDown(750);
          $('#results').css('display', 'grid');

        }
      })
    }
  })
});
